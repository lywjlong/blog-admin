package ${packageName};

import cc.xulu.common.Status;
import com.jfinal.core.Controller;

import ${modelPackageName}.*;

public class ${tableNameFirstUpper}Controller extends Controller {
	public void index() {
		setAttr("${tableName}Page", ${tableNameFirstUpper}.dao.paginate(getParaToInt(0, 1), 10, "select *", "from ${tableName} order by id asc"));
		render("list.html");
	}
	
	public void add() {
		<#list foreigns as f>
	    setAttr("${f.refName?lower_case}", ${f.refName}.dao.find("select * from ${f.refName?lower_case}"));
	    </#list>
	    render("edit.html");
	}

    public void edit() {
        <#list foreigns as f>
        setAttr("${f.refName?lower_case}", ${f.refName}.dao.find("select * from ${f.refName?lower_case}"));
        </#list>
        setAttr("${tableName}", ${tableNameFirstUpper}.dao.findById(getParaToInt()));
        render("edit.html");
    }
	
	public void save() {
		${tableNameFirstUpper} ${tableName} = getModel(${tableNameFirstUpper}.class);

		${tableName}.save();

        renderJson(${tableName});
	}

    public void show() {
        ${tableNameFirstUpper} ${tableName} = ${tableNameFirstUpper}.dao.findById(getParaToInt());

        renderJson(${tableName});

    }
	
	public void update() {
        ${tableNameFirstUpper} ${tableName} = getModel(${tableNameFirstUpper}.class);
        ${tableName}.update();
        renderJson(new Status());
	}
	
	public void delete() {
        ${tableNameFirstUpper}.dao.deleteById(getParaToInt());
        renderJson(new Status());
	}
}

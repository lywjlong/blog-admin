package cc.xulu.controller;

import cc.xulu.common.Status;
import com.jfinal.core.Controller;

import cc.xulu.model.*;

public class UserController extends Controller {
	public void index() {
		setAttr("userPage", User.dao.paginate(getParaToInt(0, 1), 10, "select *", "from user order by id asc"));
		render("list.html");
	}
	
	public void add() {
	    render("edit.html");
	}
	
	public void save() {
		User user = getModel(User.class);

		user.save();

		renderJson(user);
	}

    public void show() {
        User user = User.dao.findById(getParaToInt());

        renderJson(user);

    }
	
	public void edit() {
		setAttr("user", User.dao.findById(getParaToInt()));
		render("edit.html");
	}
	
	public void update() {
		User user = getModel(User.class);

        user.update();

        renderJson(new Status());
	}
	
	public void delete() {
		User.dao.deleteById(getParaToInt());
		renderJson(new Status());
	}
}

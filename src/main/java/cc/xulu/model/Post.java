package cc.xulu.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model; 

public class Post extends Model<Post>{

    private static final long serialVersionUID = 1L;
    public static final Post dao = new Post();
    
    public Category getCategory() {
    	return Category.dao.findById(get("category_id"));
    }
    public User getUser() {
    	return User.dao.findById(get("user_id"));
    }
 }
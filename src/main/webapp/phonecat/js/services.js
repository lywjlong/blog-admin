/**
 * Created by Xulu on 2014/6/11.
 */
angular.module('phonecatServices', ['ngResource']).factory('Services', function($resource){
   return {phone:$resource('phones/:phoneId.json',{},{query:{method:'GET',params:{phoneId:'phones'},isArray:true}}),
       tag:$resource('/blog-admin/tag/:id',{id:'@id'},{jsonp:{method:'JSONP'},query:{method:'GET',params:{id:'phones'},isArray:true},add:{method:'POST'}})
   };
});